package be.vDAB.restaurantApp.menu;

import be.vDAB.restaurantApp.classes.MenuItem;
import be.vDAB.restaurantApp.classes.RestMenu;
import be.vDAB.restaurantApp.classes.RestaurantImpl;
import be.vDAB.restaurantApp.classes.RestaurantList;
import be.vDAB.restaurantApp.utilities.Keyboard;


/**
 * This is the main menu where users can use the app and all its choices.
 * All choices are put in separate methods.
 * Everytime a user needs to make a choice, this choice is also put in a separate method
 * so that the integer changes with the choices.
 */
public class StartMenu {
    Keyboard keyboard = new Keyboard();
    RestMenu menu = new RestMenu();
    MenuItem dish = new MenuItem();
    RestaurantImpl restaurant = new RestaurantImpl();

    public void completeMenu() {
        printStartMenu();
        SelectCustomerOrOwner();
        keyboard.closeKeyboard();
    }

    /**
     * WelcomeText
     */
    public void printStartMenu() {
        prettyLines();
        System.out.printf("Welcome to the Aperture Food App %n" +
                "We are starting up the mainmenu for you %n");
        prettyLines();
    }

    public void SelectCustomerOrOwner() {
        mainMenuChoices();
    }

    /**
     * This is a method that represents the main menu
     * There is a choice between being a customer or a owner
     * you can also quit the application in this menu
     */
    private void mainMenuChoices() {
        int choiceMenu = mainMenuSelectChoice();
        if (choiceMenu == 0 || choiceMenu > 3) {
            System.out.println("Please enter a number from 1 to 3");
            mainMenuChoices();
        }

        while (choiceMenu < 3 && choiceMenu != 0) {
            if (choiceMenu == 1) {
                RestaurantList menuUitbater = new RestaurantList();
                customerChoiceMenu(menuUitbater);
                clientChoiceOrderMore(menuUitbater);
                choiceMenu = mainMenuSelectChoice();
            }

            if (choiceMenu == 2) {
                RestaurantList menuUitbater = new RestaurantList();
                getOwnerMenuChoices(menuUitbater);
                choiceMenu = mainMenuSelectChoice();
            }
        }

        if (choiceMenu == 3) {
            prettyLines();
            prettyLines();
            System.out.println("Quitting app...");
            prettyLines();
            prettyLines();

        }

    }

    private int mainMenuSelectChoice() {
        return getChoiceMenu("-----MAINMENU-----" + "\n" +
                "Please select the correct number:" + "\n" +
                "1. I am a customer" + "\n" +
                "2. I am an owner of a restaurant" + "\n" +
                "3. Please quit app");
    }

    /**
     * This is a method where we ask the customer if they want to order more dishes: 1 is yes and 2 is no.
     * @param menuUitbater
     */
    private void clientChoiceOrderMore(RestaurantList menuUitbater) {
        int choiceOrderMore = choiceOrderMore();
        if (choiceOrderMore > 2 || choiceOrderMore == 0) {
            System.out.println("Please enter 1 or 2.");
            clientChoiceOrderMore(menuUitbater);
        }
        while (choiceOrderMore < 2 && choiceOrderMore != 0) {
            if (choiceOrderMore == 1) {
                customerChoiceMenu(menuUitbater);
                choiceOrderMore = choiceOrderMore();
            }
        }
        if (choiceOrderMore == 2) {
            prettyLines();
            System.out.println("Returning to Main Menu...");
            prettyLines();
        }
    }

    /**
     * This method asks for a choice in the owner menu
     * "1. see a list of your restaurants"
     * "2. select a restaurant for adjustment"
     * "3. add a restaurant"
     * "4. Go back to mainmenu"
     * @param menuUitbater
     */
    private void getOwnerMenuChoices(RestaurantList menuUitbater) {
        int choiceOwnerMenuChoice = choiceOwnerMenu();
        while (choiceOwnerMenuChoice < 4 && choiceOwnerMenuChoice != 0) {
            if (choiceOwnerMenuChoice == 1) {
                menuUitbater.printRestaurants();
                prettyLines();
                System.out.println("Going back to Owner Menu...");
                prettyLines();
                choiceOwnerMenuChoice = choiceOwnerMenu();
            }


            if (choiceOwnerMenuChoice == 2) {
                int choiceRestaurant = menuUitbater.makeChoiceRestaurant("Please select a restaurant:");
                if (menuUitbater.getChosenRestaurant() == null) {
                    System.out.println("This is not a valid option");
                    choiceRestaurant = menuUitbater.makeChoiceRestaurant("Please select a restaurant: ");
                }
                getOwnerChoiceRestaurantMenu(menuUitbater);
                choiceOwnerMenuChoice = choiceOwnerMenu();
            }

            if (choiceOwnerMenuChoice == 3) {
                restaurant.initialiseRestaurantImpl();
                menuUitbater.addRestaurant(restaurant);
                System.out.println("This is your new list of restaurants:");
                menuUitbater.printRestaurants();
                prettyLines();
                System.out.println("Going back to Owner Menu...");
                prettyLines();
                choiceOwnerMenuChoice = choiceOwnerMenu();

            }
        }

        if (choiceOwnerMenuChoice > 4 || choiceOwnerMenuChoice == 0) {
            System.out.println("Please enter a number from 1 to 4.");
            choiceOwnerMenuChoice = choiceOwnerMenu();
        } else {
            prettyLines();
            System.out.println("Returning to Main Menu...");
            prettyLines();
        }
    }

    /**
     * This method asks for a choice in the restaurantmenu:
     * "1. change the name of the restaurant" (not available yet)
     * "2. change the address of the restaurant" (not available yet)
     * "3. see the menu of available dishes"
     * "4. options of dishes: adding, removing or adjusting"
     * "5. Go back to owner menu"
     * @param menuUitbater
     */
    private void getOwnerChoiceRestaurantMenu(RestaurantList menuUitbater) {
        int choiceRestaurantMenu = ownerChoiceRestaurantMenu();
        if (choiceRestaurantMenu > 5 || choiceRestaurantMenu == 0) {
            System.out.println("Please enter a number from 1 to 5. ");
            choiceRestaurantMenu = ownerChoiceRestaurantMenu();
        }
        while (choiceRestaurantMenu < 5 && choiceRestaurantMenu != 0) {
            if (choiceRestaurantMenu == 1) {
                //naam veranderen van resto
                System.out.println("This option isn't available yet");
                prettyLines();
                System.out.println("Going back to Restaurant Menu...");
                prettyLines();
                choiceRestaurantMenu = ownerChoiceRestaurantMenu();
            }
            if (choiceRestaurantMenu == 2) {
                //adres veranderen van resto
                System.out.println("This option isn't available yet");
                prettyLines();
                System.out.println("Going back to Restaurant Menu...");
                prettyLines();
                choiceRestaurantMenu = ownerChoiceRestaurantMenu();
            }
            if (choiceRestaurantMenu == 3) {
                menu.printMenu();
                prettyLines();
                System.out.println("Going back to Restaurant Menu...");
                prettyLines();
                choiceRestaurantMenu = ownerChoiceRestaurantMenu();
            }

            if (choiceRestaurantMenu == 4) {
                getOwnerDishesMenuChoices(menuUitbater, menu);
                choiceRestaurantMenu = ownerChoiceRestaurantMenu();

            }
        }

        if (choiceRestaurantMenu == 5) {
            prettyLines();
            System.out.println("Going back to Owner Menu...");
            prettyLines();
        }
    }

    private void prettyLines() {
        System.out.println("----------------------------------------------");
    }

    /**
     * This method asks for a choice in the dishesmenu
     * "1. Add dishes to the menu?"
     * "2. Remove dishes from the menu?"
     * "3. Adjust dishes from the menu?"
     * "4. Go back to restaurantmenu"
     * @param menuUitbater
     * @param menu
     */
    private void getOwnerDishesMenuChoices(RestaurantList menuUitbater, RestMenu menu) {
        int choiceDishesMenu = ownerChoiceDishesMenu(menuUitbater);
        if (choiceDishesMenu > 4 || choiceDishesMenu == 0) {
            System.out.println("Please enter a number from 1 to 4.");
            choiceDishesMenu = ownerChoiceDishesMenu(menuUitbater);
        }
        while (choiceDishesMenu < 4 && choiceDishesMenu != 0) {
            if (choiceDishesMenu == 1) {
                dish.initialiseMenuItem();
                menu.addMenuItem(dish);
                System.out.println("This is your menu: ");
                menu.printMenu();
                prettyLines();
                System.out.println("Going back to Dishes Menu...");
                prettyLines();
                choiceDishesMenu = ownerChoiceDishesMenu(menuUitbater);
            }
            if (choiceDishesMenu == 2) {
                menu.removeMenuItem();
                System.out.println("This is your menu: ");
                menu.printMenu();
                prettyLines();
                System.out.println("Going back to Dishes Menu...");
                prettyLines();
                choiceDishesMenu = ownerChoiceDishesMenu(menuUitbater);
            }
            if (choiceDishesMenu == 3) {
                menu.adjustMenuItem();
                System.out.println("This is your menu: ");
                menu.printMenu();
                prettyLines();
                System.out.println("Going back to DishesMenu...");
                prettyLines();
                choiceDishesMenu = ownerChoiceDishesMenu(menuUitbater);
            }
        }

        if (choiceDishesMenu == 4) {
            prettyLines();
            System.out.println("Going back to Restaurant Menu...");
            prettyLines();
        }
    }


    private int choiceOrderMore() {
        return getChoiceMenu("Would you like to order more?" + "\n" +
                "1. Yes" + "\n" +
                "2. No");
    }

    private int ownerChoiceRestaurantMenu() {
        return getChoiceRestaurantMenu("-----RESTAURANT MENU-----" + "\n" +
                "Would you like to:" + "\n" +
                "1. change the name of the restaurant?" + "\n" +
                "2. change the address of the restaurant?" + "\n" +
                "3. see the menu of available dishes?" + "\n" +
                "4. options of dishes: adding, removing or adjusting" + "\n" +
                "5. Go back to owner menu");
    }


    private int choiceOwnerMenu() {
        return getChoiceOwnerMenu("-----OWNER MENU-----" + "\n" +
                "Would you like to:" + "\n" +
                "1. see a list of your restaurants?" + "\n" +
                "2. select a restaurant for adjustment?" + "\n" +
                "3. add a restaurant" + "\n" +
                "4. Go back to mainmenu");
    }

    private int ownerChoiceDishesMenu(RestaurantList menuUitbater) {
        return getChoiceDishesMenu("-----DISHES MENU-----" + "\n" +
                "Would you like to:" + "\n" +
                "1. Add dishes to the menu?" + "\n" +
                "2. Remove dishes from the menu?" + "\n" +
                "3. Adjust dishes from the menu?" + "\n" +
                "4. Go back to restaurantmenu");
    }

    /**
     * the customer can choose a restaurant, once chosen they can choose a dish from this restaurant.
     * @param menuUitbater
     */
    private void customerChoiceMenu(RestaurantList menuUitbater) {
        int choiceRestaurant = menuUitbater.makeChoiceRestaurant("Please choose a restaurant: ");
        if (menuUitbater.getChosenRestaurant() == null) {
            System.out.println("This is not a valid option");
            choiceRestaurant = menuUitbater.makeChoiceRestaurant("Please choose a restaurant: ");
        }
        System.out.println("Cutting veggies, seasoning, prepping dishes" + "\n" +
                "----------------------------------------" + "\n");
        int choiceDish = menu.selectMenuItem("Please select a dish to order: ");
        if (menu.getSelectedMenuItem() == null) {
            System.out.println("This is not a valid option");
            choiceDish = menu.selectMenuItem("Please select a dish to order: ");
        }
        System.out.println("You have chosen: " + menu.getSelectedMenuItem());
    }


    public int getChoiceMenu(String text) {
        int choiceMenu = keyboard.readInt(text);
        return choiceMenu;
    }

    public int getChoiceDishesMenu(String text) {
        int choiceDishesMenu = keyboard.readInt(text);
        return choiceDishesMenu;
    }

    public int getChoiceRestaurantMenu(String text) {
        int choiceRestaurantMenu = keyboard.readInt(text);
        return choiceRestaurantMenu;
    }

    public int getChoiceOwnerMenu(String text) {
        int choiceOwnerMenuChoice = keyboard.readInt(text);
        return choiceOwnerMenuChoice;
    }


}
