package be.vDAB.restaurantApp.classes;

import be.vDAB.restaurantApp.interfaces.Restaurant;
import be.vDAB.restaurantApp.utilities.InputUser;


/**
 * This is the class to classify a restaurant
 * Every restaurant has a unique name and address
 * a restaurant can be put in a restaurant list
 *
 * @see RestaurantList
 * There is one default restaurant so the app can run when there is a client and not an owner.
 */

public class RestaurantImpl extends InputUser implements Restaurant {
    private String name;
    private String address;
    RestMenu menu;


    public RestaurantImpl() {

    }

    public RestaurantImpl(String name, String address) {
        setName(name);
        setAddress(address);
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Restaurant: " +
                "name: " + name +
                ", address: " + address;
    }

    public void setMenu(RestMenu menu) {
        this.menu = menu;
    }

    public void initialiseRestaurantImpl() {
        setName(askNameOfRestaurant());
        setAddress(askAddressOfRestaurant());
    }

    @Override
    public void getMenu() {

    }
}
