package be.vDAB.restaurantApp.classes;

import be.vDAB.restaurantApp.interfaces.Menu;
import be.vDAB.restaurantApp.utilities.InputUser;
import be.vDAB.restaurantApp.utilities.Keyboard;


/**
 * This is the class to classify a Menu
 * Every menu has a list of menuItems
 *
 * @see MenuItem
 * There is one default dish so the app can run when there is a client and not an owner.
 * There is a maximum of 10 menuItems on a list.
 * you can add menuItems
 * you can remove menuItems
 * you can adjust the name or price or everything of a menuItem in the list.
 * you can select menuItems
 */
public class RestMenu extends InputUser implements Menu {
    //    Fields
    private MenuItem[] menuItems;
    private int numberOfMenuItems = 10;
    private int choice;
    Keyboard keyboard = new Keyboard();


    public RestMenu() {
        menuItems = new MenuItem[numberOfMenuItems];
        menuItems[0] = new MenuItem("Pineapple pizza", "The best pizza in the world", 5.50);
    }


    public MenuItem[] getMenuItems() {
        return menuItems;
    }

    public int getNumberOfMenuItems() {
        return numberOfMenuItems;
    }

    public void setNumberOfMenuItems(int numberOfMenuItems) {
        this.numberOfMenuItems = numberOfMenuItems;
    }

    public void addMenuItem(MenuItem[] menuItems) {
        for (MenuItem menuItem : menuItems) {
            addMenuItem(menuItem);
        }
    }

    @Override
    public void addMenuItem(MenuItem menuItem) {
        if (findEmptyPlaceInMenuItems() < menuItems.length) {
            this.menuItems[findEmptyPlaceInMenuItems()] = menuItem;
            System.out.println("Your dish is added");
        } else {
            System.out.println("You can't add anymore dishes!");
        }
    }

    private int findEmptyPlaceInMenuItems() {
        for (int i = 0; i < menuItems.length; i++) {
            if (menuItems[i] == null) {
                return i;
            }
        }
        return numberOfMenuItems + 1;
    }

    public int selectMenuItem(String text) {
        printMenu();
        choice = keyboard.readInt(text);
        if (choice == 0) {
            System.out.println("0 is not an option you can choose");
            choice = keyboard.readInt(text);
        }
        return choice;
    }

    public MenuItem getSelectedMenuItem() {
        return menuItems[choice - 1];
    }

    @Override
    public void removeMenuItem() {
        selectMenuItem("Please select the number of the dish you want to remove:");
        if (menuItems[choice - 1] != null) {
            for (int i = 0; i < menuItems.length; i++) {

                menuItems[choice - 1] = null;
            }
        } else {
            System.out.println("There is no dish to remove on the number you chose");
        }
    }

    public void printMenu() {
        for (int i = 1; i <= menuItems.length; i++) {
            if (menuItems[i - 1] != null) {
                System.out.println(i + ". " + menuItems[i - 1]);
            }
        }
    }

    @Override
    public void adjustMenuItem() {
        selectMenuItem("Please select the number of the dish you want to adjust:");
        if (menuItems[choice - 1] != null) {
            int choiceAdjust = keyboard.readInt("What do you want to change? Please enter a number:" + "\n" + "1. Name"
                    + "\n" + "2. Price" + "\n" + "3. Everything of the dish");

            if (choiceAdjust == 1) {
                menuItems[choice - 1].setName(getSelectedMenuItem().askNameOfDish());
                System.out.println("This is the endresult of your adjusted dish: " + menuItems[choice - 1]);
            }
            if (choiceAdjust == 2) {
                menuItems[choice - 1].setPrice(getSelectedMenuItem().askPriceOfDish());
                System.out.println("This is the endresult of your adjusted dish: " + menuItems[choice - 1]);
            }
            if (choiceAdjust == 3) {
                MenuItem newMenuItem = new MenuItem();
                newMenuItem.initialiseMenuItem();
                menuItems[choice - 1] = newMenuItem;
                System.out.println("This is your new dish: " + menuItems[choice - 1]);

            } else {
                System.out.println("Going back to menu");
            }
        } else {
            System.out.println("There is no dish to adjust on the number you chose");
        }
    }
}
