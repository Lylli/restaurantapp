package be.vDAB.restaurantApp.classes;


import be.vDAB.restaurantApp.classes.RestaurantImpl;
import be.vDAB.restaurantApp.utilities.Keyboard;
/**
 * This is the class to classify a List of restaurants
 *
 * @see RestaurantImpl
 * There is one default restaurant so the app can run when there is a client and not an owner.
 * There is a maximum of 5 restaurants that can be on the list.
 * you can add restaurants
 * you can choose a restaurant
 *
 */
public class RestaurantList {
    private RestaurantImpl[] restaurants;
    Keyboard keyboard = new Keyboard();
    int choice;

    public RestaurantList() {
        restaurants = new RestaurantImpl[5];
        restaurants[0]= new RestaurantImpl("Chell", "verzonnenstraat 90 te 9260 Serskamp");
    }

    public RestaurantImpl[] getRestaurants() {
        return restaurants;
    }

    public void addRestaurant(RestaurantImpl[] restaurants) {
        for (RestaurantImpl restaurant : restaurants) {
            addRestaurant(restaurant);
        }
    }

    public void addRestaurant(RestaurantImpl restaurant) {
        if (findEmptyPlaceInRestaurants() < restaurants.length) {
            this.restaurants[findEmptyPlaceInRestaurants()] = restaurant;
            System.out.println("Your restaurant is added");
        } else {
            System.out.println("You can't add anymore restaurants!");
        }
    }

    private int findEmptyPlaceInRestaurants() {
        for (int i = 0; i < restaurants.length; i++) {
            if (restaurants[i] == null) {
                return i;
            }
        }
        return 6;
    }

    public void printRestaurants() {
        for (int i = 1; i <= restaurants.length; i++) {
            if (restaurants[i-1] != null){
            System.out.println(i + ". " + restaurants[i - 1]);
            }
        }
    }

    public int makeChoiceRestaurant(String text){
        printRestaurants();
        choice = keyboard.readInt(text);
        if (choice == 0){
            System.out.println("0 is not an option you can choose");
            choice = keyboard.readInt(text);
        }
        return choice;
    }
    public RestaurantImpl getChosenRestaurant(){
        return restaurants[choice-1];
    }

}