package be.vDAB.restaurantApp.classes;

import be.vDAB.restaurantApp.interfaces.Item;
import be.vDAB.restaurantApp.utilities.InputUser;

/**
 * This is the class to classify a dish
 * Every dish has a unique name, description and price
 * a dish can be put in a menu
 *
 * @see RestMenu
 * orderThisItem has not been coded yet, this will be for in the future where we can add it to a cart.
 */
public class MenuItem extends InputUser implements Item {
    String name;
    String description;
    double price;

    public MenuItem() {
    }

    public MenuItem(String name, String description, double price) {
        setName(name);
        setDescription(description);
        setPrice(price);
    }


    @Override
    public void orderThisItem() {
//        hier gaan we de gerechten bestellen, dus als een gebruiker klikt op orderthisitem
//        dan komt orderthisitem terecht in een array van complete order die afgeprint moet kunnen worden
//        deze array van CompleteOrder is een array van menuItems en dus onderdeel van restaurant
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;

    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void initialiseMenuItem() {
        setName(askNameOfDish());
        setDescription(askDescriptionOfDish());
        setPrice(askPriceOfDish());
    }

    @Override
    public String toString() {
        return "Dish: " +
                "name: " + name +
                ", description:" + description +
                ", price: " + price +
                " €";
    }


}
