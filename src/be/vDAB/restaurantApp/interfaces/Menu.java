package be.vDAB.restaurantApp.interfaces;

import be.vDAB.restaurantApp.classes.MenuItem;

/**
 * This is the interface for Menu's:
 * each menu can have menuItems added
 * each menu can have menuItems removed
 * each menu can have menuItems adjusted
 */

public interface Menu {
    void addMenuItem(MenuItem menuItem);
    void adjustMenuItem();
    void removeMenuItem();
}
