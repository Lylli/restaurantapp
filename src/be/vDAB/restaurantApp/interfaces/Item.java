package be.vDAB.restaurantApp.interfaces;

/**
 * This is an interface for Items which is for future implementations
 * @see be.vDAB.restaurantApp.classes.MenuItem
 * each item can be ordered (when the code is written for it)
 */
public interface Item {
    void orderThisItem();

}
