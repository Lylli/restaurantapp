package be.vDAB.restaurantApp.utilities;

import java.util.Scanner;

/**
 * This is a utility class that makes a keyboard for our users to use as input.
 * it also checks for 2 exceptions so users can't enter wrong characters.
 */
public class Keyboard {
    static Scanner keyboard = new Scanner(System.in);

    public static int readInt(String text) {
        System.out.println(text);
        while (true) {
            try {
                int result = Integer.parseInt(keyboard.nextLine());
                return result;
            } catch (NumberFormatException nfe) {
                System.out.println("Please enter a number");

            }
        }
    }

    public static String readLine(String text) {
        System.out.println(text);
        String line = keyboard.nextLine();
        return line;
    }

    public static double readDouble(String text) {
        System.out.println(text);
        while (true) {
            try {
                double result = Double.parseDouble(keyboard.nextLine());
                return result;
            } catch (NumberFormatException nfe) {
                System.out.println("Please enter a number where you use '.' as a comma instead of ','");

            }
        }
    }

    public static void closeKeyboard() {
        keyboard.close();
    }

}
