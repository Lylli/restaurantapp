package be.vDAB.restaurantApp.utilities;

/**
 * This is a class where the user gives input that might be asked in the app to set variables.
 * Remark: not everything is listed here yet.
 * @see Keyboard
 */
public class InputUser {
    Keyboard keyboard = new Keyboard();

    public String askNameOfDish() {
        String name = keyboard.readLine("What is your dish called?");
        return name;
    }

    public String askDescriptionOfDish() {
        String description = keyboard.readLine("Please give a description of your dish:");
        return description;
    }

    public double askPriceOfDish() {
        double price = keyboard.readDouble("What is the price of your dish?");
        return price;
    }

    public String askNameOfRestaurant() {
        String name = keyboard.readLine("Wat is your restaurant called?:");
        return name;
    }

    public String askAddressOfRestaurant() {
        String address = keyboard.readLine("Where is your restaurant located?:");
        return address;
    }
}
